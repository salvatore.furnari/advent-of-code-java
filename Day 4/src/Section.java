import java.util.ArrayList;
import java.util.HashSet;
import java.util.stream.Collectors;

public class Section {
    private String rawSection;
    private HashSet<String> section;
    public Section(String rawSection){
        this.rawSection = rawSection;
        this.section = new HashSet<>();
    }
    public HashSet<String> sectionDeserializer(){
        String lowerBound = rawSection.split("\\-")[0];
        String upperBound = rawSection.split("\\-")[1];
        for(int i = Integer.parseInt(lowerBound); i<=Integer.parseInt(upperBound); i++){
            this.section.add(String.valueOf(i));
        }
        return (HashSet<String>) this.section.clone();
    }

    @Override
    public String toString() {
        String result = "";
        for(String s : this.section){
            result = result + " " + s;
        }
        return result;
    }
}
