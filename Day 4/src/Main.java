import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

class  FileTransformer {
    private List<String> lines;

    public FileTransformer(String fileName) throws IOException {
        this.lines = Files.readAllLines(Paths.get(fileName));
    }

    public List<List<Section>> transform(){
        return lines.stream().map((l)->{
            Section section1 = new Section(l.split(",")[0]);
            Section section2 = new Section(l.split(",")[1]);
            section1.sectionDeserializer();
            section2.sectionDeserializer();
            ArrayList<Section> result = new ArrayList<>();
            result.add(section1);
            result.add(section2);
            return result;
        }).collect(Collectors.toList());
    }
}
public class Main {
    public static void main(String[] args) {
        String filename = "Day 4/src/day4file.txt";
        AtomicReference<Integer> counterFull = new AtomicReference<>(0);
        AtomicReference<Integer> counterPartial = new AtomicReference<>(0);
        try {
            new FileTransformer(filename).transform().forEach((l)->{
                Elf elf1 = new Elf(l.get(0));
                Elf elf2 = new Elf(l.get(1));
                if (elf1.isSectionFullyContained(elf2) || elf2.isSectionFullyContained(elf1)){
                    System.out.print(elf1.getSection() + "...");
                    System.out.println(elf2.getSection());
                    counterFull.updateAndGet(v -> v + 1);
                }
                if (elf1.isSectionPartiallyContained(elf2) || elf2.isSectionPartiallyContained(elf1)) {
                    System.out.print(elf1.getSection() + "...");
                    System.out.println(elf2.getSection());
                    counterPartial.updateAndGet(v -> v + 1);
                }
            });
            System.out.println("The number of total sections fully contained is: " + counterFull);
            System.out.println("The number of total sections partial contained is: " + counterPartial);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}