public class Elf {
    private Section section;
    public Elf(Section section){
        this.section = section;
    }
    public Section getSection(){
        return this.section;
    }

    public Boolean isSectionFullyContained(Elf elf2){
        for(String s: this.section.sectionDeserializer()){
            if (!elf2.getSection().sectionDeserializer().contains(s)) return false;
        }
        return true;
    }
    public Boolean isSectionPartiallyContained(Elf elf2){
        for(String s: this.section.sectionDeserializer()){
            if (elf2.getSection().sectionDeserializer().contains(s)) return true;
        }
        return false;
    }
}
