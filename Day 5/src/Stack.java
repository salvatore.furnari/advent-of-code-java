import java.util.ArrayList;

public class Stack {
    private ArrayList<String> crates;

    public Stack(ArrayList<String> crates){
        this.crates = new ArrayList<>(crates);
    }

    public String removeTopCrate(){
        String lastCrater = this.crates.get(0);
        this.crates.remove(0);
        return lastCrater;
    }
    public String removeCrate(int index){
        String crater = this.crates.get(index);
        this.crates.remove(index);
        return crater;
    }

    public String getTopCrater(){
        if (this.crates.size()>0)
            return this.crates.get(0);
        return "";
    }

    public void move(Stack s2, Integer numberOfCrates){
        for(int i=0; i < numberOfCrates; i++){
            this.crates.add(0, s2.removeTopCrate());
        }
    }

    public void move2(Stack s2, Integer numberOfCrates){
        for(int i=numberOfCrates -1; i >= 0; i--){
            this.crates.add(0, s2.crates.get(i));
            System.out.println("Crates:" + this.crates);
        }
        for(int i=0; i<numberOfCrates; i++){
            s2.removeTopCrate();
        }
    }
    @Override
    public String toString() {
        return this.crates.toString();
    }
}
