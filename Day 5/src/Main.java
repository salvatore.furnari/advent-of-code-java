import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

class  FileTransformer {
    private List<String> lines;

    public FileTransformer(String fileName) throws IOException {
        this.lines = Files.readAllLines(Paths.get(fileName));
    }

    public ArrayList<Stack> getStacks() {
        SortedMap<Integer, ArrayList<String>> mapStacks = new TreeMap<>();
        for(String l: lines){

            Pattern pattern = Pattern.compile("\\s*\\[(\\w)\\]\\s*");
            Matcher matcher = pattern.matcher(l);
            while (matcher.find()) {
                Integer key = matcher.start(1);
                System.out.println("group 1: " + matcher.group(1) + " length: " + matcher.group(1).length() + " . Pos:" + matcher.start(1) + "-" + matcher.end(1));
                if (mapStacks.containsKey(key)){
                    mapStacks.get(key).add(matcher.group(1));
                }
                else{
                    mapStacks.put(key, new ArrayList<>());
                    mapStacks.get(key).add(matcher.group(1));
                }
        }
        }
        ArrayList<Stack> stacks = new ArrayList<>();
        for(Integer key: mapStacks.keySet()){
            System.out.println(key);
            ArrayList<String> elem = new ArrayList<>(mapStacks.get(key));
            stacks.add(new Stack(elem));
        }
        return stacks;
    }
    public ArrayList<ArrayList<Integer>> getActions(){
        ArrayList<ArrayList<Integer>> allActions = new ArrayList<>();
        for(String l: lines){
            Pattern pattern = Pattern.compile("move (\\d+) from (\\d+) to (\\d+)");
            Matcher matcher = pattern.matcher(l);
            while (matcher.find()){
                ArrayList<Integer> actions = new ArrayList<>();
                actions.add(Integer.valueOf(matcher.group(1)));
                actions.add(Integer.valueOf(matcher.group(2)));
                actions.add(Integer.valueOf(matcher.group(3)));
                allActions.add(actions);
            }
        }
        return allActions;
    }
}
public class Main {
    public static void main(String[] args) {
        String filename="Day 5/src/day5file.txt";
        try {
            FileTransformer ft = new FileTransformer(filename);
            ArrayList<Stack> stacks = ft.getStacks();
            for(Stack s: stacks){
                System.out.println(s);
            }
            for(ArrayList<Integer> al : ft.getActions()){
                Integer nOfCrates = al.get(0);
                Integer s1Index = al.get(2);
                Integer s2Index = al.get(1);
                stacks.get(s1Index - 1).move2(stacks.get(s2Index - 1), nOfCrates);
                for(Stack s: stacks){
                    System.out.println(s);
                }
                System.out.println("---------");
            }
            String tops = "";
            for (Stack s : stacks){
                tops = tops + s.getTopCrater();
            }
            System.out.println(tops);


        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}