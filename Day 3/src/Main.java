import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

class  FileTransformer {
    private List<String> lines;

    public FileTransformer(String fileName) throws IOException {
        this.lines = Files.readAllLines(Paths.get(fileName));
    }

    public List<String> transform(){
        return lines.stream().map((l) -> {
            Integer rucksackLength = l.length() / 2;
            String compartment1 =  l.substring(0, rucksackLength);
            String compartment2 = l.substring(rucksackLength);
            assert compartment2.length() == compartment1.length();
            return compartment1+":"+compartment2;
        }).collect(Collectors.toList());
    }
}

public class Main {
    public static void main(String[] args) {
        String fileName = "Day 3/src/day3file.txt";
        int totalPriorityBadges = 0;
        try {
            List<String> rucksacks = new FileTransformer(fileName).transform();
            int counter = 0;
            Rucksack rucksack1 = null;
            Rucksack rucksack2 = null;
            for (String rucksack : rucksacks) {
                if (counter % 3 == 0) rucksack1 = new Rucksack(rucksack.split(":")[0], rucksack.split(":")[1]);
                else if (counter % 3 == 1) rucksack2 = new Rucksack(rucksack.split(":")[0], rucksack.split(":")[1]);
                else {
                    Integer priorityBadgeFound = new Rucksack(rucksack.split(":")[0], rucksack.split(":")[1]).findBadgeinGroup(rucksack1, rucksack2);
                    totalPriorityBadges += priorityBadgeFound;
                }
                counter += 1;
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        //System.out.println("The total priority is: " + totalPriority);
        System.out.println("The total priority of all badges is: " + totalPriorityBadges);
    }
}