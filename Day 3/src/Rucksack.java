import java.util.ArrayList;
import java.util.Collections;

public class Rucksack {
    private String compartment1;
    private String compartment2;

    public String getCompartment1() {
        return compartment1;
    }

    public String getCompartment2() {
        return compartment2;
    }
    public Rucksack(String compartment1, String compartment2){
        this.compartment1 = compartment1;
        this.compartment2 = compartment2;
    }

    private Integer getPriority(char c){
        Integer index = (int) c;
        if (index > 97) return index - 96;
        return index - 38;
    }

    public Integer checkCommonElements(){
        char[] chars = compartment1.toCharArray();

        for(char c: chars){

            if (!compartment2.contains(String.valueOf(c))) {
                continue;
            }
            System.out.println(c + "::" + (int) c);

            return getPriority(c);
        }
    return 0;
    }

    public Integer findBadgeinGroup(Rucksack r2, Rucksack r3){
        char[] chars = (compartment1 + compartment2).toCharArray();
        ArrayList<Character> charArray1= new ArrayList<>();
        for(char c: chars){
            if((r2.compartment1 + r2.compartment2).contains(String.valueOf(c))){
                charArray1.add(c);
            }
        }
        for(char c: charArray1){
            if((r3.compartment1 + r3.compartment2).contains(String.valueOf(c))){
                return getPriority(c);
            }
        }
        return null;
    }

}
