import java.awt.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class CaloriesCounter {
    private List<String> lines = null;
    public CaloriesCounter(String fileName) throws IOException {
        lines = Files.readAllLines(Paths.get(fileName));
    }

    public ArrayList<Integer> getAllCaloriesCarriedByElves(){
        Integer elfCounter = 1;
        ArrayList<Integer> temporalSnacks = new ArrayList<>();
        ArrayList<Integer> listOfTotalCalories = new ArrayList<>();
        for (String line : lines){
            if(line.length() != 0) {
                temporalSnacks.add(Integer.parseInt(line));
            }
            if(line.length() == 0){
                System.out.println("Elf " + String.valueOf(elfCounter));
                temporalSnacks.forEach((n) -> System.out.println(n));

                Elf elf = new Elf(temporalSnacks);
                Integer caloriesCarriedByTheElf = elf.getTotalCaloriesCarried();
                listOfTotalCalories.add(caloriesCarriedByTheElf);
                System.out.println("Total Calories Carried: " + String.valueOf(caloriesCarriedByTheElf));
                System.out.println("==========\n\n");
                temporalSnacks.clear();
                elfCounter++;
            }
        }
        return listOfTotalCalories;
    }
}
