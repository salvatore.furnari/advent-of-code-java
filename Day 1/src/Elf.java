import java.util.ArrayList;

public class Elf {
    private ArrayList<Integer> snacks;

    public Elf(ArrayList<Integer> snacks){
        this.snacks = new ArrayList<>();
        this.snacks.addAll(snacks);
    }

    public Integer getTotalCaloriesCarried(){
        return snacks.stream().reduce(0, (a, b)-> a + b);
    }

}
