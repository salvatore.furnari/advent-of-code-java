import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        String filename = "Day 1/src/elvesCalory.txt";
        CaloriesCounter caloriesCounter;

        {
            try {
                caloriesCounter = new CaloriesCounter(filename);
                ArrayList<Integer> totalCaloriesCarried = new ArrayList<>();
                totalCaloriesCarried = caloriesCounter.getAllCaloriesCarriedByElves();
                totalCaloriesCarried.sort((o1, o2) -> o2.compareTo(o1));
                System.out.println("The elf with most calories brings " + String.valueOf(totalCaloriesCarried.get(0)) + " calories");
                System.out.println("Second place goes to: " + String.valueOf(totalCaloriesCarried.get(1)));
                System.out.println("Third place goes to:  " + String.valueOf(totalCaloriesCarried.get(2)));
                System.out.println("The sum of the Elves with most calories is: " + String.valueOf(totalCaloriesCarried.get(0) + totalCaloriesCarried.get(1) + totalCaloriesCarried.get(2)));
            } catch (IOException e) {
                System.err.println(e.getMessage());
                throw new RuntimeException(e);
            }
        }
    }
}