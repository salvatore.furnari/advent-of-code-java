import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

class  FileTransformer {
    private List<String> lines;

    public FileTransformer(String fileName) throws IOException {
        this.lines = Files.readAllLines(Paths.get(fileName));
    }

    public List<List<Element>> transform() {
        return lines.stream().map((l) -> {
            String s1 = l.split(" ")[0];
            String s2 = l.split(" ")[1];
            Element e1 = null;
            Element e2 = null;
            switch (s1) {
                case "A": {
                    e1 = new Rock();
                    break;
                }
                case "B": {
                    e1 = new Paper();
                    break;
                }
                case "C": {
                    e1 = new Scissors();
                }
            }
            switch (s2) {
                case "X": {
                    e2 = new Rock();
                    break;
                }
                case "Y": {
                    e2 = new Paper();
                    break;
                }
                case "Z":
                    e2 = new Scissors();
            }
            List<Element> returnList = new ArrayList<>();
            returnList.add(e1);
            returnList.add(e2);
            return returnList;
        }).collect(Collectors.toList());
    }
}

class  FileTransformer2 {
    private List<String> lines;

    public FileTransformer2(String fileName) throws IOException {
        this.lines = Files.readAllLines(Paths.get(fileName));
    }

    public List<List<Element>> transform() {
        return lines.stream().map((l) -> {
            String s1 = l.split(" ")[0];
            String s2 = l.split(" ")[1];
            Element e1 = null;
            Element e2 = null;
            switch (s1) {
                case "A": {
                    e1 = new Rock();
                    break;
                }
                case "B": {
                    e1 = new Paper();
                    break;
                }
                case "C": {
                    e1 = new Scissors();
                }
            }
            switch (s2) {
                case "X": {
                    switch(s1){
                        case "A": {
                            e2 = new Scissors();
                            break;
                        }
                        case "B": {
                            e2 = new Rock();
                            break;
                        }
                        case "C": {
                            e2 = new Paper();
                            break;
                        }
                    }
                    break;
                }
                case "Y": {
                    switch(s1){
                        case "A": {
                            e2 = new Rock();
                            break;
                        }
                        case "B": {
                            e2 = new Paper();
                            break;
                        }
                        case "C": {
                            e2 = new Scissors();
                            break;
                        }
                    }
                    break;
                }
                case "Z":
                    switch(s1){
                        case "A": {
                            e2 = new Paper();
                            break;
                        }
                        case "B": {
                            e2 = new Scissors();
                            break;
                        }
                        case "C": {
                            e2 = new Rock();
                            break;
                        }
                    }
                    break;
            }
            List<Element> returnList = new ArrayList<>();
            returnList.add(e1);
            returnList.add(e2);
            return returnList;
        }).collect(Collectors.toList());
    }
}

public class Main {

    public static void main(String[] args) {
        String fileName = "Day 2/src/day2file.txt";
        RockPaperScissorsGame game = new RockPaperScissorsGame();
        AtomicReference<Integer> totalResult = new AtomicReference<>(0);
        try {
            List<List<Element>> elements = new FileTransformer2(fileName).transform();
            elements.forEach((match) -> {
                TreeMap<String, Object> result = game.play(match.get(1), match.get(0));
                System.out.println(result.get("RESULT") + "::" + result.get("SCORE"));
                totalResult.updateAndGet(v -> v + (Integer) result.get("SCORE"));
            });

            System.out.println("The total score is: " + totalResult);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }


    }
}