import java.util.Objects;
import java.util.TreeMap;

public class RockPaperScissorsGame {

    public TreeMap<String, Object> play(Element e1, Element e2){
        TreeMap<String, Object> mapResult = new TreeMap<>();
        String result = null;
        if (Objects.equals(e1.getScore(), e2.getScore())){
            result = "DRAW";
        }
        else {
            result = (e1 instanceof Rock && e2 instanceof Paper) ||
                    (e1 instanceof Paper && e2 instanceof Scissors) ||
                    (e1 instanceof Scissors && e2 instanceof Rock) ? "DEFEAT" : "VICTORY";
        }
        mapResult.put("RESULT", result);
        mapResult.put("SCORE", this.calculateScore(e1, result));
        return (TreeMap<String, Object>) mapResult.clone();
    }
    private Integer calculateScore(Element e1, String result){
        Integer matchScore = 0;
        switch (result){
            case "DEFEAT": {
                matchScore = 0;
                break;
            }
            case "VICTORY":{
                matchScore = 6;
                break;
            }
            case "DRAW":{
                matchScore = 3;
                break;
            }
        }
        return e1.getScore() + matchScore;
    }
}
