public abstract class Element {
    protected Integer score;
    protected String name;

    public Integer getScore() {
        return score;
    }
    public String getName(){
        return name;
    }
}
