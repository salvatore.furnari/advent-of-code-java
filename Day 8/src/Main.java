import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

class  FileTransformer {
    private List<String> lines;

    public FileTransformer(String fileName) throws IOException {
        this.lines = Files.readAllLines(Paths.get(fileName));
    }

    public Forest rowIterator() {
        Forest forest = new Forest(lines.get(0).length());
        this.lines.forEach((l) -> {
            forest.addRow(l);
        });
        return forest;
    }
}
public class Main {
    public static void main(String[] args) {
        String filename = "Day 8/src/day8file.txt";
        try {
            Forest forest = new FileTransformer(filename).rowIterator();
            System.out.println(forest.calculateNumberOfVisibleTrees());
            System.out.println(forest.calculateHighestScenicScore());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}