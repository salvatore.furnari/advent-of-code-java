import java.lang.reflect.Array;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class Forest {
    private ArrayList<String> grid;
    private Integer rowSize;
    private Integer columnSize;
    public Forest(ArrayList<String> grid){
        this.grid = new ArrayList<>();
        this.rowSize = grid.get(0).length();
        grid.forEach((row) -> {
            this.grid.add(row);
            columnSize++;
        });
    }


    private String getColumn(Integer index){
        return String.join("", grid.stream().map(row -> String.valueOf(row.charAt(index))).collect(Collectors.toList()));
    }
    public Boolean isVisible(String s, Integer x, Integer y){
        String xRow = this.grid.get(y);
        String yRow = this.getColumn(x);
        System.out.println("The x grid is: " + this.grid.get(y) + ". The y grid is: " + this.getColumn(x));
        if(x.equals(columnSize) || x == 0 || y == 0 || y.equals(rowSize - 1)){
            return true;
        }
        // Calculate tallest right
        Boolean x_tallest_right = Arrays.stream(xRow.substring(x  + 1).split("")).filter(e -> Integer.parseInt(e) >= Integer.parseInt(s)).toList().isEmpty();
        Boolean y_tallest_right = Arrays.stream(yRow.substring(y + 1).split("")).filter(e -> Integer.parseInt(e) >= Integer.parseInt(s)).toList().isEmpty();
        Boolean x_tallest_left = Arrays.stream(xRow.substring(0, x).split("")).filter(e -> Integer.parseInt(e) >= Integer.parseInt(s)).toList().isEmpty();
        Boolean y_tallest_left = Arrays.stream(yRow.substring(0, y).split("")).filter(e -> Integer.parseInt(e) >= Integer.parseInt(s)).toList().isEmpty();


        System.out.println("Elem:  " + s + "  X RIght:  " + x_tallest_right + " X left: " + x_tallest_left + " Elem:  " + s + "  y RIght:  " + y_tallest_right + " Y left: " + y_tallest_left);
        System.out.println("Is visible: " + (x_tallest_left || x_tallest_right || y_tallest_left || y_tallest_right));

        return x_tallest_left || x_tallest_right || y_tallest_left || y_tallest_right;
    }

    public Forest(Integer rowSize){
        this.columnSize = -1;
        this.grid = new ArrayList<>();
        this.rowSize = rowSize;
    }

    public void addRow(String row){
        this.grid.add(row);
        columnSize++;
    }

    public Integer calculateScenicScore(String s, Integer x, Integer y){
        System.out.println("Elem : " + s + " X: " + x + " Y: " +y);

        String xRow = this.grid.get(y);
        String yRow = this.getColumn(x);
        if(x.equals(columnSize) || x == 0 || y == 0 || y.equals(rowSize - 1)){
            return 0;
        }
        // Calculate tallest right
        List<String> xRowRight = Arrays.asList(xRow.substring(x  + 1).split(""));
        List<String> yRowRight = Arrays.asList(yRow.substring(y + 1).split(""));
        List<String> xRowLeft = Arrays.asList(xRow.substring(0, x).split(""));
        List<String> yRowLeft = Arrays.asList(yRow.substring(0, y).split(""));

        ListIterator<String> xr = xRowRight.listIterator();
        ListIterator<String> yr = yRowRight.listIterator();
        ListIterator<String> xl = xRowLeft.listIterator(xRowLeft.size());
        ListIterator<String> yl = yRowLeft.listIterator(yRowLeft.size());

        Integer x_tallest_right = 0;
        Integer y_tallest_right =0;
        Integer x_tallest_left = 0;
        Integer y_tallest_left = 0;

        System.out.println("Row X " + xRow);
        System.out.println("Row y " + yRow);

        if(x == 2 && y == 3){
          int u = 0;
        }

        while (xr.hasNext()){
            x_tallest_right++;
            if(Integer.parseInt(xr.next()) >= Integer.parseInt(s)) break;
        }

        while (yr.hasNext()){
            y_tallest_right++;
            if(Integer.parseInt(yr.next()) >= Integer.parseInt(s)) break;
        }

        while (yl.hasPrevious()){
            y_tallest_left++;
            if(Integer.parseInt(yl.previous()) >= Integer.parseInt(s)) break;
        }

        while (xl.hasPrevious()){
            x_tallest_left++;
            if(Integer.parseInt(xl.previous()) >= Integer.parseInt(s)) break;
        }


        if(x_tallest_left == 0) x_tallest_left++;
        if(y_tallest_left == 0) y_tallest_left++;
        if(x_tallest_right == 0) x_tallest_right++;
        if(y_tallest_right == 0) y_tallest_right++;

        System.out.println(x_tallest_left + "  " +  x_tallest_right + "  " + y_tallest_left + "  " + y_tallest_right);

        return x_tallest_left * x_tallest_right * y_tallest_left * y_tallest_right;

    }
    public Integer calculateHighestScenicScore(){
        AtomicReference<Integer> highstScenicScore = new AtomicReference<>(0);
        AtomicReference<Integer> cIt = new AtomicReference<>(0);
        AtomicReference<Integer> rIt = new AtomicReference<>(0);
        grid.forEach((row) ->{
            for(String s2:row.split("")){
                if(calculateScenicScore(s2, rIt.get(), cIt.get()) > highstScenicScore.get()){
                    highstScenicScore.set(calculateScenicScore(s2, rIt.get(), cIt.get()));
                }
                rIt.getAndSet(rIt.get() + 1);
            }
            cIt.getAndSet(cIt.get() + 1);
            rIt.set(0);
        });
        return highstScenicScore.get();
    }

    public Integer calculateNumberOfVisibleTrees(){
        AtomicReference<Integer> visibleTrees = new AtomicReference<>(0);
        AtomicReference<Integer> cIt = new AtomicReference<>(0);
        AtomicReference<Integer> rIt = new AtomicReference<>(0);
        grid.forEach((row) ->{
            for(String s2:row.split("")){
                if(isVisible(s2, rIt.get(), cIt.get())) {
                    visibleTrees.getAndSet(visibleTrees.get() + 1);
                }
                rIt.getAndSet(rIt.get() + 1);
            }
            cIt.getAndSet(cIt.get() + 1);
            rIt.set(0);
        });
        return visibleTrees.get();
    }

    @Override
    public String toString() {
        AtomicReference<String> s = new AtomicReference<>("");
        grid.forEach((row) ->{
            s.set(s.get() + row + "\n");
        });
        return s.get();
    }
}
