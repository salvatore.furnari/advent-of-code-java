import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

class  FileTransformer {
    private List<String> lines;

    public FileTransformer(String fileName) throws IOException {
        this.lines = Files.readAllLines(Paths.get(fileName));
    }

    public void scanFS() {
        FileSystem fs = new FileSystem();
        String currentState = "";
        for(String l: lines){
            if (l.startsWith("$")){
                if (l.startsWith("$ cd")){
                    fs.cd(l.split("\\$ cd")[1].trim());
                    currentState = "cd";
                    System.out.println(fs.getCurrentPath());
                }
                if (l.startsWith("$ ls")){
                    currentState = "ls";
                }
            }
            else{
                if (currentState.equals("ls")){
                    String elem1 = l.split(" ")[0];
                    String elem2 = l.split(" ")[1];
                    if (elem1.equals("dir")){
                        ElfDirectory d = new ElfDirectory(elem2);
                        fs.add(d);
                    }
                    else{
                        ElfFile ef = new ElfFile(elem2, Integer.valueOf(elem1));
                        fs.add(ef);
                    }
                }
            }
        }
        fs.toString();
        System.out.println(fs.geSizeOfDirs(100000));
        System.out.println(fs.findBiggestFolderToDelete());
    }
}
public class Main {
    public static void main(String[] args) {
        String filename = "Day 7/src/day7file.txt";
        try {
            new FileTransformer(filename).scanFS();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}