public abstract class FSElement {
    protected Boolean isDir;
    protected Integer size;
    protected String name;
    public FSElement(String name, Boolean isDir, Integer size){
        this.name = name;
        this.isDir = isDir;
        this.size = size;
    }

    @Override
    public String toString() {
        return "NAME: " + name + "  ISDIR: " + isDir + "  SIZE:  " + size;
    }
}
