import java.util.*;
import java.util.stream.Collectors;

public class FileSystem {
    private TreeMap<String, ArrayList<FSElement>> filesystem;
    private TreeMap<String, Integer> sizes;
    private String currentPath;
    private static final int totalSpace = 70000000;

    public FileSystem(){
        filesystem = new TreeMap<>();
        filesystem.put("/", new ArrayList<>());
        sizes = new TreeMap<>();
        sizes.put("/", 0);
        this.currentPath = "/";
    }

    public void cd(String path) {
        if (path.equals("/")) {
            this.currentPath = "/";
            return;
        }
        if (path.equals("..")) {
            List<String> tempPath = Arrays.asList(this.currentPath.split("/"));
            ArrayList<String> al = new ArrayList<>(tempPath);
            if (al.size() > 2) {
                al.remove(al.size() - 1);
                this.currentPath = String.join("/", al);
            }
            else this.currentPath = "/";
        } else {
            if (currentPath.length() > 1)
                this.currentPath = this.currentPath + "/" + path;
            else currentPath = this.currentPath + path;
        }
        if (!filesystem.containsKey(this.currentPath)) {
            filesystem.put(currentPath, new ArrayList<>());
            sizes.put(currentPath, 0);
        }
    }


    public void add(FSElement fsElement){
        this.filesystem.get(currentPath).add(fsElement);
        if (!fsElement.isDir){
            this.sizes.put("/", this.sizes.get("/") + fsElement.size);
            ArrayList<String> tempPath = new ArrayList<>(Arrays.asList(currentPath.split("/")));
            while(tempPath.size() > 1){
                this.sizes.put(String.join("/", tempPath), sizes.get(String.join("/", tempPath)) + fsElement.size);
                tempPath.remove(tempPath.size() - 1);
            }

        }
        System.out.println(this.filesystem.get(currentPath));
    }

    @Override
    public String toString() {
        for(String path : filesystem.keySet()){
            System.out.println(path+ "::" + filesystem.get(path));
        }
        System.out.println(this.sizes);
        return "";
    }

    public String getCurrentPath() {
        return currentPath;
    }

    public Optional<Integer> geSizeOfDirs(Integer threshold){
        return this.sizes.entrySet().stream().filter(entry -> (entry.getValue() < threshold)).collect(Collectors.toList()).stream().map(Map.Entry::getValue).collect(Collectors.toList()).stream().reduce(Integer::sum);
    }
    public Integer getFreeSpace(){
        return totalSpace - this.sizes.get("/");
    }

    public Integer findBiggestFolderToDelete(){
        Integer updateSize = 30000000;

        Integer threshold = updateSize - getFreeSpace() ;
        LinkedHashMap<String, Integer> sortedMap = sizes.entrySet().stream()
                .filter(entry -> entry.getValue() > threshold)
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey,
                        Map.Entry::getValue,
                        (e1, e2) -> e1, LinkedHashMap::new));
        for(String s: sortedMap.keySet()){
            return sortedMap.get(s);
        }
        return 0;
    }

}
