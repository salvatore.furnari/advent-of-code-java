import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

class  FileTransformer {
    private List<String> lines;

    public FileTransformer(String fileName) throws IOException {
        this.lines = Files.readAllLines(Paths.get(fileName));
    }

    public Rope rowIterator() {
        Rope rope = new Rope(new Point(30, 30), 10);
        this.lines.forEach((l) -> {
            rope.move(l.split(" ")[0], Integer.valueOf(l.split(" ")[1]));
        });
        return rope;
    }
}
public class Main {
    public static void main(String[] args) {
        String filename = "Day 9/src/day9file.txt";
        try {
            Rope rope = new FileTransformer(filename).rowIterator();
            System.out.println("Number of unique positions visited by the tail: " + rope.getNumberOfPointsVisitedByTheTail());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}