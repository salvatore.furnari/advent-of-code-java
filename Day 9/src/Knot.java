public class Knot {
    private Point position;
    private Integer value;

    public Knot(Point position, Integer value){
        this.position = new Point(position);
        this.value = value;
    }

    public Integer geXtPosition() {
        return position.getX();
    }
    public Integer getYPosition() {
        return position.getY();
    }
    public Point getPosition(){
        return position;
    }

    public Integer getValue() {
        return value;
    }
}
