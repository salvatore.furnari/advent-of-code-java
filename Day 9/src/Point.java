import java.io.Serializable;
import java.util.Objects;

public class Point implements Serializable {
    private Integer x;
    private Integer y;

    public Point(Integer x, Integer y){
        this.x = x;
        this.y = y;
    }

    public Integer getX() {
        return x;
    }

    private void checkNegative(Integer coord){
        if(coord < 0){
            System.err.println("Negative number");
        }
    }

    public Integer getY() {
        return y;
    }

    public void setX(Integer x) {
        this.x = x;
        checkNegative(this.x);
    }

    public void setY(Integer y) {
        this.y = y;
        checkNegative(this.y);
    }

    public void setRelativeX(Integer x) {
        this.x +=x;
        checkNegative(this.x);
    }

    public void setRelativeY(Integer y) {
        this.y += y;
        checkNegative(this.y);
    }

    @Override
    public String toString() {
        return "( " + x + "," + y + ")";
    }

    public Point(Point point){
        this.x = point.getX();
        this.y = point.getY();
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public Double calculateDistance(Point point){
        //System.out.println("The distance is " + Math.sqrt(Math.pow(point.getX() - x, 2) + Math.pow(point.getY() - y, 2)));
        return Math.sqrt(Math.pow(point.getX() - x, 2) + Math.pow(point.getY() - y, 2));
    }

    @Override
    public boolean equals(Object obj) {
        return Objects.equals(this.y, ((Point) obj).y) && Objects.equals(this.x, ((Point) obj).x);
    }

    @Override
    public int hashCode() {
        return Integer.parseInt(x + "" + y);
    }
}
