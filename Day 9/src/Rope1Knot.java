import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;

public class Rope1Knot {
    private Point HPos;
    private Point TPos;

    private HashSet<Point> coordinatesVisitedByTheTail;

    public Rope1Knot(Point startPos){
        HPos = new Point(startPos);
        TPos = new Point(startPos);
        this.coordinatesVisitedByTheTail = new HashSet<>();
        this.coordinatesVisitedByTheTail.add(new Point(startPos));
    }


    private void calculateTailPos(){
        Double distance = TPos.calculateDistance(HPos);
        //System.out.println("--------------");
        ArrayList<Point> points = new ArrayList<>();
        if(distance == 2.0){
            points.add(new Point(TPos.getX() - 1, TPos.getY()));
            points.add(new Point(TPos.getX() + 1, TPos.getY()));
            points.add(new Point(TPos.getX(), TPos.getY() - 1));
            points.add(new Point(TPos.getX(), TPos.getY() + 1));
        }
        else if(distance > 2.0){ //distance of the tail > 2
            points.add(new Point(TPos.getX() - 1, TPos.getY() -1));
            points.add(new Point(TPos.getX() - 1, TPos.getY() + 1));
            points.add(new Point(TPos.getX() + 1, TPos.getY() + 1));
            points.add(new Point(TPos.getX() + 1, TPos.getY() - 1));
        }
        else return;
        Double minDistance = 10.0;
        Point tempPos = new Point(TPos);
        for(Point p: points) {
            if (p.calculateDistance(HPos) < minDistance) {
                minDistance = p.calculateDistance(HPos);
                tempPos.setX(p.getX());
                tempPos.setY(p.getY());
                //System.out.println(tempPos);
            }
        }
        TPos.setX(tempPos.getX());
        TPos.setY(tempPos.getY());
        coordinatesVisitedByTheTail.add(tempPos);
        //System.out.println("THe final distance is " + minDistance + ". TPOS: " + TPos);
    }

    public void printGrid(){
        int maxX = Math.max(HPos.getX(), TPos.getX());
        int maxY = Math.max(HPos.getY(), TPos.getY());
        for(int y=0; y<=20; y++){
            for(int x=0; x<20; x++){
                if(HPos.getX() == x && HPos.getY() == y) System.out.print("H");
                else if(TPos.getX() == x && TPos.getY() == y) System.out.print("T");
                else System.out.print(".");
            }
            System.out.println();
        }
        System.out.println("\n\n");
    }


    public void printTPosGrid(){
        int maxX = Math.max(HPos.getX(), TPos.getX());
        int maxY = Math.max(HPos.getY(), TPos.getY());
        for(int y=0; y<=10; y++){
            for(int x=0; x<10; x++){
                if(coordinatesVisitedByTheTail.contains(new Point(x, y))){
                    System.out.print("#");
                }
                else System.out.print(".");
            }
            System.out.println();
        }
        System.out.println("\n\n");
    }

    private void moveHeadX(Integer increment, Point endPos){
        while (!Objects.equals(HPos.getX(), endPos.getX())){
            HPos.setRelativeX(increment);
            System.out.println("HPOS: " + HPos + " TPOS: " + TPos);
            calculateTailPos();
            printGrid();
        }
    }

    private void moveHeadY(Integer increment, Point endPos){
        while (!Objects.equals(HPos.getY(), endPos.getY())){
            HPos.setRelativeY(increment);
            System.out.println("HPOS: " + HPos + " TPOS: " + TPos);
            calculateTailPos();
            printGrid();
        }
    }
    public void move(String direction, Integer steps){
        Point endPos = new Point(HPos);
        switch (direction){
            case "L":{
                endPos.setRelativeX(steps * -1);
                moveHeadX(-1, endPos);
                break;
            }
            case "R":{
                endPos.setRelativeX(steps);
                moveHeadX(1, endPos);
                break;
            }
            case "U":{
                endPos.setRelativeY(steps * -1);
                moveHeadY(-1, endPos);
                break;
            }
            case "D":{
                endPos.setRelativeY(steps);
                moveHeadY(1, endPos);
                break;
            }
        }


    }

    public Integer getNumberOfPointsVisitedByTheTail(){
        printTPosGrid();
        return this.coordinatesVisitedByTheTail.size();
    }


}
