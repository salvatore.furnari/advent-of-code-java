import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;
import java.util.Vector;

public class Rope {

    private HashSet<Point> coordinatesVisitedByTheTail;
    private Vector<Knot> knots;

    public Rope(Point startPos, Integer numberOfKnots){
        knots = new Vector<>(numberOfKnots);
        for(int i=0; i<numberOfKnots;i++){
            knots.add(new Knot(startPos, i));
        }
        this.coordinatesVisitedByTheTail = new HashSet<>();
        this.coordinatesVisitedByTheTail.add(new Point(startPos));
    }

    private void calculateTailPos(Integer index, Point HPos, Point TPos){
        Double distance = TPos.calculateDistance(HPos);
        //System.out.println("--------------");
        ArrayList<Point> points = new ArrayList<>();
        if(distance == 2.0){
            points.add(new Point(TPos.getX() - 1, TPos.getY()));
            points.add(new Point(TPos.getX() + 1, TPos.getY()));
            points.add(new Point(TPos.getX(), TPos.getY() - 1));
            points.add(new Point(TPos.getX(), TPos.getY() + 1));
        }
        else if(distance > 2.0){ //distance of the tail > 2
            points.add(new Point(TPos.getX() - 1, TPos.getY() -1));
            points.add(new Point(TPos.getX() - 1, TPos.getY() + 1));
            points.add(new Point(TPos.getX() + 1, TPos.getY() + 1));
            points.add(new Point(TPos.getX() + 1, TPos.getY() - 1));
        }
        else return;
        Double minDistance = 10.0;
        Point tempPos = new Point(TPos);
        for(Point p: points) {
            if (p.calculateDistance(HPos) < minDistance) {
                minDistance = p.calculateDistance(HPos);
                tempPos.setX(p.getX());
                tempPos.setY(p.getY());
                //System.out.println(tempPos);
            }
        }
        TPos.setX(tempPos.getX());
        TPos.setY(tempPos.getY());
        if(index == this.knots.size() - 1)
            coordinatesVisitedByTheTail.add(tempPos);
        //System.out.println("THe final distance is " + minDistance + ". TPOS: " + TPos);
    }


    public Integer isKnotPresent(Point point){
        for(Knot k: knots){
            if(Objects.equals(k.getYPosition(), point.getY()) && Objects.equals(k.geXtPosition(), point.getX()))
                return k.getValue();
        }
        return -1;
    }

    public void printTPosGrid(){
        for(int y=0; y<=20; y++){
            for(int x=0; x<20; x++){
                if(coordinatesVisitedByTheTail.contains(new Point(x, y))){
                    System.out.print("#");
                }
                else System.out.print(".");
            }
            System.out.println();
        }
        System.out.println("\n\n");
    }

    public void printGrid(){
        for(int y=0; y<=10; y++){
            for(int x=0; x<10; x++){
                if(isKnotPresent(new Point(x, y))!= -1){
                    System.out.print(isKnotPresent(new Point(x, y)));
                }
                else System.out.print(".");
            }
            System.out.println();
        }
        System.out.println("\n\n");
    }

    private void moveHeadX(Integer increment, Point endPos){
        while (!Objects.equals(knots.get(0).geXtPosition(), endPos.getX())){
            knots.get(0).getPosition().setRelativeX(increment);
            int index = 1;
            while (index < knots.size()){
                calculateTailPos(index, knots.get(index - 1).getPosition(), knots.get(index).getPosition());
                index++;
            }
            printGrid();
        }
    }

    private void moveHeadY(Integer increment, Point endPos){
        while (!Objects.equals(knots.get(0).getYPosition(), endPos.getY())){
            knots.get(0).getPosition().setRelativeY(increment);
            int index = 1;
            while (index < knots.size()){
                calculateTailPos(index, knots.get(index - 1).getPosition(), knots.get(index).getPosition());
                index++;
            }
            printGrid();
        }
    }
    public void move(String direction, Integer steps){
        Point endPos = new Point(knots.get(0).getPosition());
        switch (direction) {
            case "L" -> {
                endPos.setRelativeX(steps * -1);
                moveHeadX(-1, endPos);
            }
            case "R" -> {
                endPos.setRelativeX(steps);
                moveHeadX(1, endPos);
            }
            case "U" -> {
                endPos.setRelativeY(steps * -1);
                moveHeadY(-1, endPos);
            }
            case "D" -> {
                endPos.setRelativeY(steps);
                moveHeadY(1, endPos);
            }
        }


    }

    public Integer getNumberOfPointsVisitedByTheTail(){
        printTPosGrid();
        return this.coordinatesVisitedByTheTail.size();
    }


}
