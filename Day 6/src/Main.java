import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

class  FileTransformer {
    private List<String> lines;

    public FileTransformer(String fileName) throws IOException {
        this.lines = Files.readAllLines(Paths.get(fileName));
    }

    public List<Datastream> getDataStreams() {
        return lines.stream().map(Datastream::new).collect(Collectors.toList());
    }
}
public class Main {
    public static void main(String[] args) {
        String filename = "Day 6/src/day6file.txt";
        try {
            List<Integer> results = new FileTransformer(filename).getDataStreams().stream().map(Datastream::findStartMarker).collect(Collectors.toList());
            System.out.println(results);
            List<Integer> results2 = new FileTransformer(filename).getDataStreams().stream().map(Datastream::findMessageMarker).collect(Collectors.toList());
            System.out.println(results2);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}