import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Datastream {
    private String dataStream;

    public Datastream(String dataStream){
        this.dataStream = dataStream;
    }

    private Boolean hasDuplicates(String substring){
        List<String> list =  Arrays.asList(substring.split(""));
        Set<String> set = list.stream().map(String::valueOf).collect(Collectors.toSet());
        if (set.size() != list.size()) {
            // you have duplicate
            return true;
        }
        return false;
    }
    public Integer findStartMarker() {
        String tempString = "";
        Integer index = 1;
        for (char c : this.dataStream.toCharArray()) {
            tempString += c;
            if (tempString.length() < 5) {
                index++;
                continue;
            }
            tempString = tempString.substring(1);
            if (!hasDuplicates(tempString)) {
                return index;
            }
            index++;
        }
        return null;
    }

    public Integer findMessageMarker() {
        String tempString = "";
        Integer index = 1;
        for (char c : this.dataStream.toCharArray()) {
            tempString += c;
            System.out.println(index);
            if (tempString.length() < 15) {
                index++;
                continue;
            }
            tempString = tempString.substring(1);
            System.out.println(tempString);
            if (!hasDuplicates(tempString)) {
                return index;
            }
            index++;
        }
        return null;
    }

}
